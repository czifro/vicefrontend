/**
 * Created by czifro on 5/16/15.
 */

angular.module('Vice', [
  'ngRoute',
  'mobile-angular-ui',
  'angular-carousel'
])
  .config(function($routeProvider) {
    $routeProvider.when('/', {templateUrl:'home/home.html',  reloadOnSearch: false});
    $routeProvider.when('/team',        {templateUrl: 'team/team.html', reloadOnSearch: false});
    $routeProvider.when('/newsFeed',        {templateUrl: 'newsFeed/newsFeed.html', reloadOnSearch: false});
    $routeProvider.when('/products',          {templateUrl: 'products/products.html', reloadOnSearch: false});
    $routeProvider.when('/contactUs',     {templateUrl: 'contactUs/contactUs.html', reloadOnSearch: false});
    $routeProvider.when('/admin',     {templateUrl: 'admin/admin.html', reloadOnSearch: false});
  });
