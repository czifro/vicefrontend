/**
 * Created by czifro on 5/22/15.
 */

angular.module('Vice')
  .controller('IndexCtrl', function($scope, $rootScope) {
    $scope.logout = function(){
      $rootScope.logout();
    };
    $scope.clickBackBtn = function() {
      switch ($rootScope.option) {
        case 0:

          break;
        case 1:
          $rootScope.changeView($rootScope.view - 1, true);
          break;
      }
    }
  });
