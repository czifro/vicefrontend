/**
 * Created by czifro on 5/20/15.
 */

angular.module('Vice')

  .controller('AdminCtrl', function($scope, $rootScope, ParseService) {
    $scope.rememberMe = true;
    $scope.username = '';
    $scope.password = '';
    $scope.logged = false;
    $scope.token = '';
    $rootScope.view = 0;
    $scope.view = 0;

    $scope.setRememberMe = function () {
      $scope.rememberMe = !$scope.rememberMe;
      if ($scope.rememberMe)
        $('#toggle').addClass('active');
      else
        $('#toggle').removeClass('active');
    };

    $scope.login = function() {
      ParseService.login($scope.username, $scope.password).then(function(data){
        if (data != null) {
          $scope.token = data.sessionToken;
          if ($scope.rememberMe) {
            store($scope.token, $scope.username, $scope.password);
          }
          else {
            store('','','');
          }
          $rootScope.changeView(1, false);
          logoutBtn(true);
        }
      });
    };

    $scope.autoLogin = function() {
      $rootScope.option = 1;
      $scope.token = ParseService.loadToken();
      ParseService.validateToken($scope.token).then(function(data){
        if (data != null){
          $rootScope.changeView(1, false);
          logoutBtn(true);
          $('#admin-content').removeClass('hidden');
          return;
        }
        var user = angular.fromJson(ParseService.getCredentials());
        if (typeof user != 'undefined') {
          $scope.username = user.username;
          $scope.password = user.password;
        }
        $('#admin-content').removeClass('hidden');
      });
    };

    $scope.autoLogin();

    $rootScope.logout = function(){
      $rootScope.changeView(0, false);
      logoutBtn(false)
      if ($scope.rememberMe) {
        var user = angular.fromJson(ParseService.getCredentials());
        $scope.username = user.username;
        $scope.password = user.password;
        store('', $scope.username, $scope.password);
      }
      else {
        store('','','');
      }
    };

    $rootScope.changeView = function(view, fromBackBtn) {
      if (view > 1 && fromBackBtn) {
        $rootScope.view = 1;
        backBtn(false);
      }
      else if (view < 0) {
        $rootScope.view = 0;
      }
      else {
        if (view > 1)
          backBtn(true);
        $rootScope.view = view;
      }
      $scope.view = $rootScope.view;
    };

    var store = function(token, u, p) {
      ParseService.storeToken(token);
      ParseService.storeCredentials({username: u, password: p});
    };

    var logoutBtn = function(show) {
      if (show) {
        $('#searchBtn').addClass('hidden');
        $('#logoutBtn').removeClass('hidden');
      }
      else {
        $('#searchBtn').removeClass('hidden');
        $('#logoutBtn').addClass('hidden');
      }
    };

    var backBtn = function(show) {
      if (show) {
        $('#menuBtn').addClass('hidden');
        $('#backBtn').removeClass('hidden');
      }
      else {
        $('#menuBtn').removeClass('hidden');
        $('#backBtn').addClass('hidden');
      }
    };

    $scope.productCategories = [
      {
        name: 'Bars'
      },
      {
        name: 'Decks'
      },
      {
        name: 'Pegs'
      }
    ];

    $scope.productCategory = 'Product Type...';

    $scope.chooseCategory = function(val, id) {
      $scope.productCategory = val;
    }
  });
