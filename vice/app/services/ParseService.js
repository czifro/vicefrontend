/**
 * Created by czifro on 5/16/15.
 */

'use strict';

angular.module('Vice')
  .factory('ParseService', function($http){

    var baseUrl = "https://api.parse.com/1";

    var ParseService = {
      name: 'Parse',

      storeToken: function storeToken(token){
        localStorage.token = token;
      },

      loadToken: function loadToken() {
        return localStorage.token;
      },

      storeCredentials: function storeCredentials(user) {
        localStorage.user = angular.toJson(user);
      },

      getCredentials: function getCredentials() {
        return localStorage.user;
      },

      login: function login(username, password) {
        var innerConfig = {
          url: baseUrl + "/login",
          method: "GET",
          params: { username: username, password: password },
          headers: {
            'X-Parse-Application-Id': 'q0bQXhWgJtpjMOXGwI6WbOJmQiVNcKdyur33Clye',
            'X-Parse-REST-API-Key': '8S5bJ8o4ylX8Usiw73XHCuRkdv6VSRqqNb9VaN3x'
          }
        };

        return $http(innerConfig).then(onSuccess, requestFailed);

        function onSuccess(results) {
          results = results;
          if (results && results.data) {
            return results.data;
          }
          return null;
        }
      },

      validateToken: function validateToken(token){
        var innerConfig = {
          url: baseUrl + "/users/me",
          method: "GET",
          headers: {
            'X-Parse-Application-Id': 'q0bQXhWgJtpjMOXGwI6WbOJmQiVNcKdyur33Clye',
            'X-Parse-REST-API-Key': '8S5bJ8o4ylX8Usiw73XHCuRkdv6VSRqqNb9VaN3x',
            'X-Parse-Session-Token': token
          }
        };

        return $http(innerConfig).then(onSuccess, requestFailed);

        function onSuccess(results) {
          if (results && results.data) {
            return results.data;
          }
          return null;
        }
      },

      getNewsItems: function getNewsItems() {
        var innerConfig = {
          url: baseUrl + "/classes/NewsItem",
          method: "GET",
          headers: {
            'X-Parse-Application-Id': 'q0bQXhWgJtpjMOXGwI6WbOJmQiVNcKdyur33Clye',
            'X-Parse-REST-API-Key': '8S5bJ8o4ylX8Usiw73XHCuRkdv6VSRqqNb9VaN3x'
          }
        };
        return $http(innerConfig).then(onSuccess, requestFailed);

        function onSuccess(results)
        {
          if (results && results.data) {
            return results.data;
          }
          return null;
        }
      },

      getProductCategories: function getProductCategories()
      {
        var innerConfig = {
          url: baseUrl + "/classes/ProductCategories",
          method: "GET",
          headers: {
            'X-Parse-Application-Id': 'q0bQXhWgJtpjMOXGwI6WbOJmQiVNcKdyur33Clye',
            'X-Parse-REST-API-Key': '8S5bJ8o4ylX8Usiw73XHCuRkdv6VSRqqNb9VaN3x'
          }
        }
        return $http(innerConfig).then(onSuccess, requestFailed);

        function onSuccess(results)
        {
          if (results && results.data) {
            return results.data;
          }
          return null;
        }
      }
    };

    function requestFailed(error, error_description) {
      console.log(error)
    }

    return ParseService;
  });
