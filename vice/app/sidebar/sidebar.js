/**
 * Created by czifro on 5/16/15.
 */

angular.module('Vice')

  .controller('SidebarCtrl', function($scope) {
    $scope.facebook = 'https://www.facebook.com/pages/Riggz-indoorskatepark/328708073822453';
    $scope.youtube = 'https://www.youtube.com/user/MitchellDfilms';
    $scope.instagram = 'https://instagram.com/riggzindoorskatepark/';
  });
