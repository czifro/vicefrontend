/**
 * Created by czifro on 5/19/15.
 */

angular.module('Vice')

  .controller('HomeCtrl', function($scope, $rootScope) {
    $rootScope.showBackArrow = true;

    $scope.images = [
      {
        url: "https://lh3.googleusercontent.com/-0nXOtxTDGUo/TYk4UsH7nhI/AAAAAAAAAYo/s9v9ssRaglg/s1600/1_066.jpg"
      },
      {
        url: "http://cdn.shopify.com/s/files/1/0257/4773/products/PSWHGP10CBhr.jpg?v=1427251988"
      },
      {
        url: "http://i39.tinypic.com/2w57sew.jpg"
      },
      {
        url: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRcMIQEPKI9VZGqXjANZ_2sK7_tFk2xV6s-vRIt3YLiWnkGykN1KQ"
      }
    ];

    $scope.showBackBtn = function(){
      $('#backBtn').removeClass('hidden');
      $('#menuBtn').addClass('hidden');
    };

    //$scope.showBackBtn();
    /*$scope.getInnerWidth = function() {
      return window.innerWidth;
    }*/
  });
