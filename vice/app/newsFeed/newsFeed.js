/**
 * Created by czifro on 5/16/15.
 */

angular.module('Vice')

  .controller('NewsFeedCtrl', function($scope, ParseService){
    $scope.newsItems = [];

    $scope.getWidth = function(){
      return window.innerHeight;
    };

    $scope.screenWidth = window.innerWidth;

    var getNewsItems = function(){
      //console.log($scope);
      ParseService.getNewsItems().then(function(newsItems){
        console.log(newsItems);
        $scope.newsItems = newsItems.results;
      });
    };

    getNewsItems();
  });
