/**
 * Created by czifro on 5/16/15.
 */

angular.module('Vice')

  .controller('ProductCategoriesCtrl', function($scope, ParseService){
    $scope.productCategories = [];

    //$scope.newsItem = $scope.newsItems[0];

    var getProductCategories = function(){
      //console.log($scope);
      ParseService.getProductCategories().then(function(productCategories){
        console.log(productCategories);
        $scope.productCategories = productCategories.results;
      });
    }

    getProductCategories();
  });
